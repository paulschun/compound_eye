---
title: "Gratitude Journaling: For Health and Happiness"
posted_at: "2017-04-03T20:00:00.00+02:00[Europe/Berlin]"
location: Berlin, Germany
main_image: /images/blog/tegeler-see.jpg
description: "Starting your 750 words a day and stuck with a blank page? This post teaches the benefits of and strategies to begin gratitude journaling."

---


Can you remember the last time you started a sentence with "I'm thankful for..." and meant it? No political motivations, no buttering up your loved ones, just genuine gratitude?

Now the same question, but with frustration. Do you remember the last time you felt like your life was unfairly hard?

If you answered no and yes, respectively, to these question, you're not alone. This phenomenon is called [headwinds/tailwinds asymmetry](https://www.ncbi.nlm.nih.gov/pubmed/27869473) and basically states that people tend to pay more mind to their barriers than their blessings. This is a bit of a shame, since there are so many benefits associated with genuine gratitude! This post will explore those benefits, and then if you're convinced that you want to begin gratitude journaling, it will also explore the how-to of this great habit.

## Gratitude. It's Good For You.

<div class="img-attr">
  <img src="/images/blog/person-running.jpg" alt="An awesome person exercising." title="This could be you!" />
  <p>Image from <a href="http://maxpixel.freegreatpicture.com/Fitness-Running-Long-Distance-Female-Runner-573762" target="_blank">Max Pixel</a></p>
</div>

In an [article](http://greatergood.berkeley.edu/article/item/why_gratitude_is_good) written by Robert Emmons, the world's leading scientific expert on gratitude and professor of psychology at University of California, Davis, one can see all of the benefits of gratitude that he rattles off:

### Physical Benefits
  * Stronger immune systems
  * Less bothered by aches and pains
  * Lower blood pressure
  * Exercise more and take better care of their health
  * Sleep longer and feel more refreshed upon waking


### Psychological Benefits
  * Higher levels of positive emotions
  * More alert, alive, and awake
  * More joy and pleasure
  * More optimism and happiness


### Social Benefits
  * More helpful, generous, and compassionate
  * More forgiving
  * More outgoing
  * Feel less lonely and isolated.


A couple of them that I have personal accounts for, and perhaps you might as well, are discussed below.

### Exercise more and take better care of their health.

Yes, you read that correctly! Research results showed that [people who wrote about gratitude exercised more and had fewer visits to physicians](http://www.health.harvard.edu/newsletter_article/in-praise-of-gratitude) than people who wrote about their sources of aggravation. I certainly find that when I make the take to write, I also end up exercising more. Exercise is a major part of my life and identity, but journal time is when I catch myself with the old "I'm too busy excuse" and make sure that I make it a higher priority. This habit has also helped me shed 25 lbs (11 kg) recently!
### Sleep longer and feel more refreshed upon waking up.

The health benefits of longer and higher quality of sleep can be discussed at a different time, but to be certain: they are there, especially in the mental health department. But just as stress and environmental factors may eat away at both sleep duration and quality, gratitude dons its armor and fights back handily. In my own account, writing a bit before bed has also helped improve my sleep quality significantly, so I can back this claim up with my own experience as evidence.

## But... What _Am_ I Actually Grateful For?

If you're an avid Freakonomics podcast listener (and I'd recommend you be one, because just like Write Honey, [it's free and awesome](http://freakonomics.com/)), you've probably already listened to the episode where they discuss [gratitude and greed](http://freakonomics.com/podcast/why-is-my-life-so-hard/). If you haven't (or even if you have), I'll pull some great snippets from psychology professors Shai Davidai and Tom Gilovich that were on the episode:

> DAVIDAI: When you ask people what are you grateful for, the prototypical answer is: my parents, my family, my friends, my loved ones. What they're missing is all these invisibles.

What are the invisibles? Davidai goes on in his chat with Gilovich:

> GILOVICH: Oh, there's so many.

> DAVIDAI: The fact that I had opportunities for education.

> GILOVICH: How did I get even to be alive?

> DAVIDAI: The fact that we can sit here and talk. No one is monitoring what we're talking about. That is something that not everyone has. That is something that we should feel grateful for.



This is where a bit of critical thinking comes in. How did you get to where you are, right now? How many meals do you think you've consumed in order to provide your body with the nutrients it needs to operate as it does today? How many people did it take to produce that food? How many animals?

Getting past surface-level gratitude is the name of the game here. Nothing against your friends and family, but a deeper realization of the complexities involved in creating your current existence and all the discomfort it was intended to prevent are a great way to find what you might be thankful for. And such brainstorming will certainly accelerate the benefits mentioned above.

## Habitualizing Gratitude


<div class="img-attr">
  <img src="/images/blog/gratitude-note.jpg" alt="A note that says I'm grateful to be alive!" title="This could be your note!" />
  <p>Image from <a href="https://www.flickr.com/photos/eekim/2691580307" target="_blank">Eugene Kim</a></p>
</div>


You're sold on the benefits of gratitude and you've even identified some not-so-obvious things that you've been taking for granted. Now is the time to turn those into today's 750 words! In your favorite [writing tool](/) or notebook, go ahead and get started with the following tips.

### Make it your first sentence.

Something along the lines of "Today, I want to discuss [your topic] and why I'm so thankful for it" will work nicely. I'd suggest just leaving it alone at the top, and then get started with a new paragraph afterwards.

### Dig deep.

You identified this as being something that you are genuinely grateful for. Why? If it wasn't the case, what kinds of hardships might you encounter? Which of those hardships are the ones you really are happy to not have to deal with?

### Put that new knowledge to use constantly.

Now I'm hoping that you're familiar with the benefits associated with gratitude, are knowledgeable in how to recognize things to be thankful for, and how to habitualize it to make sure the benefits are retained. I hope that you gain the benefits mentioned above, and happy writing!

## Learn More

If you're still hungry to learn more about gratitude writing, I'd suggest taking a look at Nicole Pittman's _[The Gratitude Workbook](http://21180eftrpj2xa5a173fnf1nbg.hop.clickbank.net/)_, which provides a 30-day workbook with interactive journaling exercises.
