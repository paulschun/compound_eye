(ns compound-eye.layout
  (:require
   [compound-eye.component :as component]
   [compound-eye.image :as image]
   [hiccup.page :as hiccup]
   ))

(defonce build-time
  (System/currentTimeMillis))

(defonce scheme-and-host
  "https://writehoney.com")

(defn append-version-get-param [uri]
  (str uri "?v=" build-time))

;; Base

(defn base-layout [{:keys [title power-title content description og-type og-image path]
                    :or {title    "Free Writing, Free Forever"
                         og-type  "website"
                         og-image "/images/og-default.jpg"}
                    :as page}]
  (hiccup/html5
   {:lang "en"}
   [:head
    [:title (or power-title (str "Write Honey &mdash; " title))]
    [:link
     {:rel "icon"
      :type "image/x-icon"
      :href "/icons/favicon.ico"}]
    (for [{:keys [path dimension]} (image/png-favicons)]
      [:link {:rel "icon"
              :type "image/png"
              :href path
              :sizes (str dimension "x" dimension)}])
    (for [{:keys [path dimension]} (image/apple-touch-icons)]
      [:link {:rel "apple-touch-icon"
              :sizes (str dimension "x" dimension)
              :href path}])
    [:meta {:charset "utf-8"}]
    [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
    [:meta {:name "description" :content description}]
    [:meta {:name "robots" :content "index,follow"}]
    [:meta {:property "og:title" :content (or power-title title)}]
    [:meta {:property "og:url" :content (str scheme-and-host path)}]
    [:meta {:property "og:image" :content (str scheme-and-host (append-version-get-param og-image))}]
    [:meta {:property "og:type" :content og-type}]
    [:meta {:property "og:description" :content description}]
    [:meta {:property "og:locale" :content "en_US"}]
    [:meta {:name "theme-color" :content "#FFC500"}]
    [:link {:rel "stylesheet" :href (append-version-get-param "/css/main.css")}]
    [:link {:rel "canonical" :href (str scheme-and-host path)}]]
   [:body
    content
    [:script {:src (append-version-get-param "/js/app.js")}]]))

;; Pages

(defn- wrap-page-layout-content [content]
  [:div.Page__Wrapper
   (component/navbar)
   content
   (component/footer)])

(defn page-layout [page]
  (base-layout (update page :content wrap-page-layout-content)))

;; Blog

(defn- wrap-blog-layout-content [content]
  (wrap-page-layout-content content))

(defn blog-layout [page]
  (base-layout (update page :content wrap-blog-layout-content)))
