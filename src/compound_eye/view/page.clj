(ns compound-eye.view.page
  (:require
   [compound-eye.blog :as blog]
   [compound-eye.component :as component]
   [compound-eye.time :as time]
   ))

(defn- full-height-section [& contents]
  (let [class-name (when (string? (first contents)) (first contents))
        elements (if class-name (rest contents) contents)]
    [:section.hero.is-medium
     {:class class-name}
     [:div.hero-body.content.is-flex.is-align-items-stretch
      [:div.container.is-flex.is-flex-direction-column.is-justify-content-space-around.has-text-centeredf.is-align-items-stretch
       elements]]]))

(defn- recommenders []
  [:div.columns
   [:div.column
    [:a {:href "https://library.louisville.edu/researchers/write-honey"
         :rel "nofollow"
         :target "_blank"}
     [:img.Homepage__Logo
      {:src "/images/logos/university-of-louisville.png"
       :alt "Logo of the University of Louisville"
       :title "University of Louisville"}]]]
   [:div.column
    [:a {:href "https://guides.libraries.indiana.edu/c.php?g=983303&p=7110629"
         :rel "nofollow"
         :target "_blank"}
     [:img.Homepage__Logo
      {:src "/images/logos/indiana-university-logo.png"
       :alt "Logo of Indiana University"
       :title "Indiana University"}]]]
   [:div.column
    [:a {:href "https://www.depts.ttu.edu/provost/uwc/graduate/GWCStarterPack.php"
         :rel "nofollow"
         :target "_blank"}
     [:img.Homepage__Logo
      {:src "/images/logos/ttu-logo.png"
       :alt "Logo of Texas Tech University"
       :title "Texas Tech University"}]]]
   [:div.column
    [:a {:href "https://magazine.eacr.org/productivity-tools-for-research-writing/"
         :rel "nofollow"
         :target "_blank"}
     [:img.Homepage__Logo
      {:src "/images/logos/eacr.png"
       :alt "Logo of the European Association for Cancer Research"
       :title "European Association for Cancer Research"}]]]])

;; Index Page

(def index-page
  [:div.Homepage
   [:section.hero.is-fullheight-with-navbar.Homepage__MainHero
    [:div.hero-body.is-flex.is-align-items-stretch
     [:div.container.is-flex.is-flex-direction-column.is-justify-content-space-around.is-align-items-center.has-text-centered
      [:div
       [:div
        [:h1.Homepage__Title "Write Honey."]
        [:p.Homepage__Subtitle " Free writing for everyone, for free, forever."]]]
      [:div
       [:div.iMacScreen__Wrapper
        [:div.iMacScreen__Wallpaper
         [:div.iMacScreen__Writing
          [:span.element]]]
        [:div.iMacScreen]]]
      [:div
       [:a.button.is-primary.is-rounded
        {:href "https://app.writehoney.com/register"
         :rel "nofollow"
         :target "_blank"}
        "Create Your Free Account"]]
      ]]]

   (full-height-section
    "Homepage__Section--Yellow"
    [:h3.subtitle "We made free writing easy for you."]
    [:p "Write Honey is an application that helps you build a free writing habit."]
    [:p "Whether you're brain-dumping 750 words a day or you need a push writing your novel, we've got you covered."]
    [:p "It's free. It's private. It's secure."]
    [:p "You'll always have access to anything you've ever written."]
    [:p "And when you want to stop using it, you can download everything you've written and delete your account, no questions asked."]
    [:p
     [:a.button.is-black.is-rounded
      {:href "/product/"}
      "See the Product Overview &rarr;"]])

   (full-height-section
    [:h3.subtitle "Free writing is kind of a big deal for humanity."]
    [:p "Our guiding light is our firm belief that every person on this planet should have access to free writing and benefits associated with it."]
    [:p "And in case you weren't aware, there are loads of benefits to free writing. It's worth making a habit out of it if you ask us!"]
    [:p
     [:a.button.is-black.is-rounded
      {:href "/free-writing/"}
      "Learn More About Free Writing &rarr;"]]
    )

   (full-height-section "Homepage__Section--Yellow"
                        [:h3.subtitle "Your privacy is a very, very serious matter."]
                        [:p "Write Honey is built to make sure the only eyes on what you've written are yours."]
                        [:p "Unlike other free writing services, we find sharing your data with third parties morally reprehensible, creepy and just flat out incorrect."]
                        [:p "🇩🇪 To keep things extra secure, our servers are based in Germany, which has some of the strictest data privacy laws in the world."]
                        [:p
                         [:a.button.is-black.is-rounded
                          {:href "/product/"}
                          "Learn More In the Product Overview &rarr;"]])

   (full-height-section
    [:div
     [:h3.subtitle "We're just happy to be noticed."]
     [:p "It's a tremendous honor to be recommended by top universities and organizations around the world."]]
    (recommenders))

   (full-height-section
    "Homepage__Section--Yellow"
    [:div
     [:h3.subtitle "And those who love writing love us, too."]
     [:div.is-italic
      {:style "line-height: 2em; margin: 2em 0;"}
      "&ldquo;Write Honey makes daily writing easy and, most importantly, achievable. Whether I need a place to draft my next book chapter or just need to \"brain dump\" - Write Honey is my app of choice. I like that I can set my own daily word limits, that I'm not held accountable when I miss a day of writing, and that it's free to use. The interface is free of distractions which allows ideas to flow and writing to be an experience rather than a chore. I highly recommend Write Honey to anyone wanting to write - from the next Stephen King to anyone in need of clarity. Write Honey is where new ideas are born.&rdquo;"]
     [:div.has-text-centered
      [:p
       "&mdash; "
       [:a {:href "https://www.rikacossey.com/"
            :title "Rika Cossey"
            :target "_blank"}
         "Rika Cossey"]
        ", Coach &amp; Free Writing Teacher"]
      [:figure.image
       {:style "display: inline-block;"}
       [:a {:href "https://www.rikacossey.com/"
            :title "Rika Cossey"
            :target "_blank"}
         [:img {:src "/images/logos/rika-cossey.png"
                :alt "Logo for Rika Cossey"
                :title "Rika Cossey"
                :style "width: 200px"}]]]

      ]])

   (full-height-section
    [:div
     [:h3.subtitle "Why Not See It For Yourself?"]
     [:p "Using Write Honey is free and will always be free. Give it a try and see what it's all about."]
     [:div
       [:a.button.is-primary.is-rounded
        {:href "https://app.writehoney.com/register"
         :rel "nofollow"
         :target "_blank"}
        "Create Your Free Account"]]]
    )
   ])

(def index
  {:title "Free writing for everyone, for free, forever."
   :description "Write Honey is a free service that unlocks all benefits of free writing."
   :path "/"
   :priority 1
   :content index-page})

;; Product Page

(def product-content
  [:div
   [:section.hero.is-medium.is-black
    [:div.hero-body
     [:div.has-text-centered
      [:h1.title "About Write Honey"]]]]
   [:div.container
    [:section.section
     [:div.content.is-size-5
      [:h2 "Your habit of 750 words a day starts here."]
      [:p "Or however many words you want to write. That's completely up to you. What matters is that you build up your habit, because there are so many benefits associated with having a writing habit, and that's exactly what Write Honey exists to help you gain."]
      [:div.ActionScreenshot__Wrapper
       [:p.has-text-centered.is-size-6.is-italic "Write Honey in action."]
       [:div.ActionScreenshot
        [:img {:src "/images/writehoney-screenshot-2.png"
               :alt "A screenshot of Write Honey being used"}]]]
      [:h2 "Sunshine for your cloudy mind."]
      [:p "Those thoughts that have been swirling around in your head? They need an outlet. Stop living with this stress and declutter the mind with Write Honey. Forget about spelling and grammar - just jot the thoughts down and take a look at them once they're brought into the world to make more sense of them."]
      [:h2 "We take your trust seriously."]
      [:p "All of our technology is based in Germany, which has some of the strictest privacy laws in the world. On top of that, Write Honey is built with strong encryption, because your writing is yours and no one else's, and that's how it should be!"]
      [:h2 "Questions?"]
      [:h3 "What Is Write Honey?"]
      [:p "Write Honey is a free online tool that helps you jot down everything that's on your mind. Doing this is beneficial for many reasons:"]
      [:ul
       [:li "You think critically about how to word thoughts in your brain. This makes your thoughts more coherent."]
       [:li "You may encounter something bothering you that you weren't conscious of. You become more aware of yourself."]
       [:li "You think about the important things running in your mind and fill in details and create goals. You become more productive."]]
      [:h3 "How Does It Work?"]
      [:p "You sign in and you write. Then you keep writing until you reach your word count goal, which is 750 words by default and completely customizable. Then, the next day, your canvas is wiped clean and you do the same thing again."]
      [:h3 "Are My Posts Private?"]
      [:p "Yes! Therefore, you are strongly encouraged to not feel pressure to make sense or be grammatically correct."]
      [:h3 "What Happens to My Old Posts?"]
      [:p "They're with you as long as you keep your account and get deleted only when you delete your account."]
      [:h3 "How Much Does Write Honey Cost?"]
      [:p "Write Honey is completely free, and will always be free to write in and mentally declutter. A premium version may appear at a later date."]
      [:h3 "What if I don't want to use Write Honey but still want the benefits of free writing?"]
      [:p
       "There are other wonderful services like "
       [:a {:href "https://somedailywords.com/"
            :target "_blank"
            :rel "nofollow"} "SomeDailyWords"]
       " and otherwise, notebooks and loose pieces of paper are also wonderful ways to free write."]
      [:h3 "Who Built Write Honey?"]
      [:p
       "Write Honey is a labor of love from the team at "
       [:a {:href "https://elbower.com/"
            :target "_blank"
            :rel "nofollow"} "Elbower"]
       "."]]]]
   (component/cta)])

(def product
  {:title "About Write Honey"
   :path "/product/"
   :priority 0.9
   :description "All the questions you have about Write Honey, answered."
   :content product-content})

;; Contact Us Page - NOT USED YET

(def contact
  {:title "Contact Us"
   :path "/contact/"
   :priority 0.5
   :content
   [:div
    [:section.hero.is-medium.is-danger
     [:div.hero-body
      [:div.has-text-centered
       [:h1.title "Contact Us"]]]]
    ]})

;; 750 words alternative page

(def alternative-to-750-words-page-content
  [:div
   [:section.hero.is-medium.is-primary
    [:div.hero-body
     [:div.has-text-centered
      [:h1.title "750 Words Free Alternative: Write Honey"]
      [:h2.subtitle "Write Honey is a free alternative to 750 Words with a story."]]]]
   [:div.container
    [:section.section
     [:div.content.is-size-5
      [:h3 "Write Honey is a free alternative to 750 Words."]
      [:p "Hello, writer! If you're just looking to start using a free 750 Words alternative and don't care to know the backstory, then read no further. I won't judge. Really."]
      [:p "Happy writing!"]
      [:p.has-text-centered
       [:a.button.is-black.is-rounded
        {:href "https://app.writehoney.com/register"
         :target "_blank"
         :rel "nofollow"}
        "Create Your Free Account"]]
      [:div.ActionScreenshot__Wrapper
       [:p.has-text-centered.is-size-6 "Here's what Write Honey looks like in action."]
       [:div.ActionScreenshot
        [:img {:src "/images/writehoney-screenshot-2.png"
               :alt "A screenshot of Write Honey being used"}]]]
      [:h3 "Alright, I'll bite. Tell me more."]
      [:p "Let me start by stating the obvious: 750 Words is excellent software created and managed by good people. They've done wonderful things for the free writing community and the writing community as a whole, and for many years after launching, they offered their service for free."]
      [:p "And even when they found that they had to start charging to keep their servers running and to continue providing their service, any user who signed up for their software until that point was gifted a premium account for life."]
      [:p "So given their contributions to our niche as well as their generosity, I'd say it might be worth considering paying for their service if you can afford it."]
      [:h3 "And what if I can't or don't want to pay?"]
      [:p "If you can't or don't want to pay for 750 Words, I can totally understand because I've been there."]
      [:p "In fact, it was when I was dead broke and living in a tiny 8-square-meter room in a shared flat in Berlin in 2016 when I learned about the process of free writing through 750 Words. To say the least, my mental health wasn't in the best shape it had been in during this period of my life."]
      [:p "Always a big believer in the benefits of other forms of reflection like meditation, I tried out 750 Words and took to it right away. I never build up a massive streak, but I did see a very clear benefit when I'd write: my thoughts became tangible and visible."]
      [:p "While I like meditating and reflecting in general, while doing those activities, thoughts would come and go like a leaf in a river would. Namely, I'd find myself able to concentrate on a thought temporarily but knew it would pass by before I could understand it at a deeper level."]
      [:p "But when I'd write, it's like the leaf would come out of the river and into my hand. It became something I could dwell on, poke around, smell, taste, and scrutinize until my eyes dried."]
      [:p "Once I ran into the paywall at end of the trial, however, I knew that I wasn't able to shell out the cash for it. Not at that moment in my life, at least."]
      [:p "And I also knew that I wasn't the only person to encounter that same situation. I decided then and there that I couldn't let the benefits of free writing elude humanity for a reason as silly as money, and built the first prototype of Write Honey at the end of that year."]
      [:p "Since then, we've grown to collect recognition from top universities and organizations around the world:"]
      (recommenders)
      [:h3 "That's a nice story and all, but let's get real: why should I choose Write Honey as my free alternative to 750 Words?"]
      [:p "If I could give you three reasons to choose Write Honey as a free 750 Words alternative, it'd be these:"]
      [:h4 "1. It's Free"]
      [:p "Write Honey is proud to be free, for everyone, forever. We believe the power and benefits of free writing should be available to every person in the planet."]
      [:h4 "2. It's Simple"]
      [:p "Just sign up and start writing. You'll always be able to access all of your content for all of time."]
      [:p "And whenever you'd like, you can download all your writing and delete your account without any questions."]
      [:h4 "3. It's Secure"]
      [:p "With Write Honey, the only eyes on your content are yours. We find anything else flat out wrong and, unlike other providers, would never even consider letting third-parties touch your content. We respect the writer, plain and simple."]
      [:p "🇩🇪 Additionally, our servers are based in Germany, whose strictness around data privacy ensures that your data remains yours and no one else's."]

      [:h3 "Whichever platform you choose, please give it an honest effort."]
      [:p
       "The "
       [:a {:href "/free-writing/"} "benefits of free writing"]
       " are numerous, and I can't stress enough how a seemingly simple activity such as free writing can make a tremendous positive impact on anyone's life."]]]]
   (component/cta)])

(def alternative-to-750-words-page
  {:power-title "750 Words Free Alternative: Write Honey"
   :description "750 Words is excellent software created by good people, but it isn't free. Write Honey is free for everyone, forever."
   :path "/750-words-free-alternative/"
   :priority 0.7
   :content alternative-to-750-words-page-content})

;; Free Writing Page

(defn external-link [url text]
  [:a {:href url, :target "_blank", :rel "nofollow"} text])

(defn section-header [{:keys [text icon alt]}]
  [:section.hero.is-medium.Guide__SectionHeader
   [:div.hero-body
    [:div.container
     [:div.is-flex.is-justify-content-space-between.is-align-items-center
      [:h2.title text]
      [:figure.image.is-128x128
       [:img {:src icon
              :alt alt}]]]]]])

(def free-writing-page-content
  [:div
   [:section.hero.is-large.is-primary.Guide__Header
    [:div.hero-body
     [:div.container
      [:div.columns
       [:div.column.is-half
        [:h1.title "Free Writing: The Complete Guide"]
        [:p.subtitle.Guide "Understand what free writing is, view examples and understand how and where you can maximize the benefits of this immensely useful activity."]]]]]]
   [:section.section
    [:div.container.content.Guide
     [:p "Perhaps you heard about free writing from a friend who told you in passing that it's something that they enjoy doing or find beneficial to their life. Or maybe it was a form of therapy that was recommended to you by a professional or a relative. Or maybe you're just curious to learn more about free writing after hearing about it in an article or blog post elsewhere on the Internet."]
     [:p "Regardless, you've found yourself on what I hope is a complete and comprehensive guide to free writing, so let me go ahead and tell you what you can expect to get from reading this:"]
     [:p
      [:ul
       [:li "You'll learn what free writing is."]
       [:li "You'll learn about the benefits of free writing."]
       [:li "You'll see some examples of free writing."]
       [:li "You'll get some structured ideas and prompts on how to begin a free writing session."]
       [:li "You'll get some tips on how to make free writing a lasting habit."]
       ]]
     [:p "We hope you'll enjoy learning all about the wonderful world of free writing!"]
     ]]
   (section-header
    {:text "What Is Free Writing?"
     :icon "/images/pencil-brain.svg"
     :alt "Icon of a pencil over a brain"})
   [:section.section
    [:div.container.content.Guide
     [:p "In order to help make free writing understandable and accessible, we provide a formal definition as well as information surrounding the origins of this practice."]
     [:h3 "The Definition of Free Writing"]
     [:p "A common byproduct of the kind of informality with which free writing is typically mentioned is the proliferation of differing, and sometimes even conflicting, definitions that become assigned to it."]
     [:p "With this guide, I aim to defeat this for once and for all, by providing a formal definition of what free writing is; a definition around which I've built Write Honey."]
     [:h5 "A Formal Definition of Free Writing"]
     [:p "Here's my take on free writing:"]
     [:blockquote
      [:b "Free writing"]
      " is a form of writing where correctness and formality are eschewed in favor of, and to maximize, ideation."]
     [:p "With this broad definition, I acknowledge that there are many different variables that influence the way that one may go about free writing, and thus may consider to be part of its essence. These include, but are not limited to, the following:"
      [:ul
       [:li
        [:b "Types of ideation"]
        ": Generating ideas for new content, reflecting on the day, analyzing something to gain a deeper understanding of its nuances, etc."]
       [:li
        [:b "Medium"]
        ": On paper, in a word processor, on software like Write Honey, etc."]
       [:li
        [:b "Timing"]
        ": Writing everything within a certain amount of time, not idling for more than a certain period of time during a free writing session, etc."]
       [:li
        [:b "Word or character goals"]
        ": Not stopping until you've reached a certain amount of words or letters in a given day or week, etc."]]]
     [:p "Because these are all implementation details geared towards a central goal of ideation, I've decided that they have no business belonging in the definition of free writing itself."]
     [:h5 "Is it spelled free writing, freewriting or free-writing?"]
     [:p "After a bit of flip-flopping, I've become strongly of the opinion that the correct spelling of this activity is \"free writing.\""
      [:p "The existence of so many spelling variants of the same activity indicates to us a lack of earnestness and focus in promoting the activity itself, which is a huge shame. Free writing is an immensely beneficial and useful activity, and confusing readers by splitting content up between different spelling variants is not how I see free writing becoming a mainstay in the average person's arsenal of wellness and developmental tools."]
      [:p "If you decide to make content about free writing, I'm begging you: please use this spelling variant."]
      [:p "In any case, though, as Shakespeare may have put it: free writing by any other name would smell as sweet."]]
     [:h3 "A Brief History of Free Writing"]
     [:p "The origins of free writing begin with the origins of journaling, so it's worth taking a look at how humanity adopted this practice in the first place."]
     [:h5 "Moving Slabs of Limestone in Ancient Egypt"]
     [:p
      "While Ancient Egypt brought us miracles of humanity such as the Sphinx, it was the Egyptian monarch Khufu's commissioning of the Pyramids of Giza that introduced us to another wonder of humanity that's still present and perfectly relevant today: the diary."]
     [:p
      "Though the earliest form of writing itself is credited to Mesopotamia, where scholars agree that "
      [:a
       {:href "https://www.bl.uk/history-of-writing/articles/where-did-writing-begin"
        :target "_blank"
        :rel "nofollow"}
       "the first form of writing appeared 5,500 years ago"]
      ", it was the ancient Egyptian Merer, a middle-ranking official under Khufu, who is credited as being the first to put writing to use to record activity in the form of a diary. "]
     [:p
      [:a {:href "https://www.smithsonianmag.com/history/ancient-egypt-shipping-mining-farming-economy-pyramids-180956619/"
           :target "_blank"
           :rel "nofollow"}
       "Rediscovered in 2013 after being lost for millennia"]
      ", the sheets of papyrus now known as the Diary of Merer, also known as Papyrus Jarf, is the earliest-known writing resembling a diary on human record. Written in hieroglyphs, the documents recorded the daily activities of Merer and his crew as they transported blocks of limestone from a quarry in Tura, which today is in the southern outskirts of modern-day metropolitan Cairo, to Giza, a trek of about 25 kilometers."]
     [:p "A common misconception that this diary helped debunk is that that the pyramids were built by slaves. In fact, the documents claimed that the crew were paid well and, on top of that, were given meat, fish, poultry and beer. Perhaps some friends whose furniture I've helped move before could learn some lessons from Merer."]
     [:h5 "Divine Interventions on Your Creativity"]
     [:p "Fast-forward four-and-a-half millennia and breakthroughs such as paper mills, ballpoint pens and national education systems (and the concept of a nation itself for that matter) made the practice of journaling more accessible."]
     [:p "As demand for writing increased alongside improvements in education, and the production of writing instruments and materials began to match this demand, humanity began to understand a new nuance of writing: writer's block."]
     [:p "Though the term of writer's block is officially recognized as having been coined in 1947 by Austrian psychoanalyst Edmund Bergler, the effects of writer's block have been known to humanity well before this term was created."]
     [:p
      "Per "
      (external-link "https://www.newyorker.com/magazine/2004/06/14/blocked" "the New Yorker")
      ", one of the first known cases of writer's block came from early Romantic poet Coleridge, who, in 1804, compared his sudden inability to produce great poetry to the arms of a paralyzed man. Early Romantics like Coleridge saw poetry as something conferred from an external, magical source, and once they lost their ability to create poetry, decided it was the due to a power that was out of their control."]
     [:p "While Bergler's coining of the term writer's block finally shed light to this curious phenomenon, his attribution to its root cause wasn't exactly scientific: a mother who bottle fed and an unstable private life were among his list of reasons one may have writer's block."]
     [:h5 "Peter Elbow's "
      [:i "Writing Without Teachers"]
      " (1973)"]
     [:p
      "A few decades after Bergler came up with the term writer's block, Peter Elbow, Professor of English at the University of Massachusetts, published "
      [:i "Writing Without Teachers"]
      ", his challenge to this phenomenon."]
     [:p "Equipped with his belief that \"everyone in the world wants to write,\" he penned two different techniques to defeat writer's block, the more accessible of the two being what he dubbed free writing. His approach was relatively structured: practice writing for ten minutes daily, without stopping or editing, leaving the latter as the final step in the writing process."]
     [:p "His motivation for penning this technique can be summarized from this telling excerpt from page 26 of "
      [:i "Writing Without Teachers"]
      ":"
      ]
     [:blockquote "It is simply a fact that most of the time you can't find the right words till you know exactly what you are saying, but you can't know exactly what you are saying till you find just the right words. The consequence is that you must start by writing the wrong meanings in the wrong words; but keep writing until you get to the right meanings in the right words. Only in the end will you know what you are saying."]
     [:p "Elbow's contribution of the free writing technique was based on his own empirical evidence, not from scientific research, and in turn became the subject of which  many research papers were written. The majority of the results of these papers show a wide range of benefits, which are covered extensively in the next section."]
     ]]
   (section-header
    {:text "Benefits of Free Writing"
     :icon "/images/lightbulb-brain.svg"
     :alt "Icon of a lightbulb in a person's brain area"})
   [:section.section
    [:div.container.content.Guide
     [:p "You may have heard that free writing happens to have many, many wonderful benefits. With this section, I hope to show you what they actually are. I aim to persuade you to consider this worthwhile activity by digging through the work of researchers on the topic of free writing, as well as providing telling statements from those who've used the technique extensively."]
     [:h3 "Insights From Research and Scholarly Articles On Free Writing"]
     [:h5 "Free Writing Fosters Your Psychological Growth"]
     [:p "While an admirable goal, it's an almost overly done cliche that one should aim to \"expand their mind.\" Still, free writing is hailed as an activity that will help you do just that. "
      ]
     [:p "In a "
      [:a {:href "https://www.jstor.org/stable/42871242"
           :target "_blank"
           :rel "nofollow"}
       "paper on free writing published by professor Alice G. Brand"]
      " at the University of Missouri-St. Louis, free writing:"]
     [:blockquote
      [:p "...seems to proceed hand in hand with psychological growth, to reflect and enhance it, to deepen and extend it, and often to quicken the process."]]
     [:h5 "Free Writing Helps You Think Critically"]
     [:p "If the old adage \"knowledge is power,\" then free writing is the clean, jerk and snatch of the mind. It is one thing to absorb an idea at face value, and another to dissect it thoroughly, examining its strengths and weaknesses and walking away that much wiser and more informed."
      ]
     [:p
      "Reinforcing free writing's ability to beef up your analytical skills, a "
      [:a
       {:href "https://files.eric.ed.gov/fulltext/EJ1244111.pdf"
        :target "_blank"
        :ref "nofollow"}
       "collaborative international research project between researchers in universities in Malaysia and Bangladesh"]
      " concluded that free writing was:"]
     [:blockquote
      [:p "...an effective tool to help think critically, stimulate ideas and build confidence in writing."]]
     [:h5 "Free Writing Helps You Improve Fluency and Decrease Anxiety In a Second Language"]
     [:p "The results of this research may not come as a surprise to some given the hype surrounding the effective results that deliberate practice brings, but when learning a second language, it may be hard to come by opportunities to use that language, particularly if you aren't located geographically in a place where the language is commonly spoken."]
     [:p
      "That's where free writing can help you. In a "
      [:a
       {:href "https://academic.oup.com/eltj/article-abstract/74/3/318/5850271"
        :target "_blank"
        :ref "nofollow"}
       "research paper published by Jeongyeon Park"]
      ", professor at Busan University of Foreign Studies, thirty non-native English-speaking students who were asked to perform free writing exercises remarkably all improved fluency in English. After completion, they largely stated that free writing also:"
      [:blockquote
       [:p "...improved their confidence, lessened their fear of evaluation, and deepened their thinking skills"]]]
     [:h5 "Free Writing Enhances Understanding and Self-Confidence in Academic Writing"]
     [:p "Before this trope gets beaten to death, I'll repeat what seems to be common knowledge across cultures: practice makes perfect. This is true in any field and practicing most anything."]
     [:p
      "That's why those who've chosen a career in academia should consider free writing. "
      [:a
       {:href "https://files.eric.ed.gov/fulltext/EJ1105378.pdf"
        :target "_blank"
        :ref "nofollow"}
       "Research from Linda Y. Li"]
      " of the University of Canberra resulted in her concluding that, when fully utilized, free writing is a useful tool for students to:"]
     [:blockquote
      [:p "...be empowered to think through problems, make discoveries, gain insights, and express themselves with confidence"]]
     [:h3 "Insights From the Free Writing Community"]
     [:h5 "Free Writing Gives You More Good Ideas"]
     [:p "Ever been uncertain if an idea was good or not, and that hesitance led to the idea never become a tangible thought? That might be you self-censoring a brilliant idea that wasn't given enough time to bake."]
     [:p
      "Eric Grunwald, a lecturer at MIT's Department of Global Studies and Languages, "
      [:a
       {:href "https://writingprocess.mit.edu/process/step-1-generate-ideas/instructions/freewriting"
        :target "_blank"
        :ref "nofollow"}
       "claimed that"]
      ", among other benefits, free writing:"]
     [:blockquote
      [:p "...increases the flow of ideas and reduces the chance that you’ll accidentally censor a good idea."]]
     [:h5 "Free Writing Offers a Cathartic Release"]
     [:p "Whether it's acute stress from events related to the COVID pandemic or generalized stress from the burdens of daily life, the therapeutic benefits of writing may help you bounce back."]
     [:p "In "
      (external-link
       "https://health.ucdavis.edu/news/headlines/writing-as-healing-offers-a-cathartic-release/2020/04"
       "a press release from the UC Davis Comprehensive Cancer Center")
      ", program manager Terri Wolf says that free writing..."
      ]
     [:blockquote
      [:p "...is a chance to be reflective, to release the pain, to just let out whatever is going on, with no critique and no judgment of any kind. It’s just you, telling your story."]]
     ]]
   (section-header
    {:text "Free Writing Examples"
     :icon "/images/open-book.svg"
     :alt "Icon of an open book"})
   [:section.section
    [:div.content.container.Guide
     [:p "Given the personal nature of free writing, we were hesitant to include examples of free writing, but decided to add a few to provide context of a what the output of a free writing session may look like. Please take these examples as a general guideline rather than a hard template of what you should expect your own output look like."]
     [:h5 "Sample from MIT's Eric Grunwald"]
     [:p
      "This sample of free writing comes from MIT's Eric Grunwald from his page, "
      (external-link "https://writingprocess.mit.edu/sample-freewrite" "The Writing Process")
      ":"
      ]
     [:blockquote
      [:p "How to begin writing a response paper? I like to take the time to think about what the article. I picked the box man. Sounds interesting. Reminds me of a story I read for taks about some thing with a girl who was asking around about something and getting peoples philosophies about life. One person was terrified of the “box”. Boxes everywhere people living in boxes driving in boxes always trapped in the box. That lady was probably homeless or something. I dunno. Boxman, boxman, boxman. I wonder what this article is actually about. I have no idea, really. I need to read it a few times. Right now it is a mystery. I hate this! The mystery of the box man. Maybe he’s isolated himself from society, or maybe he was isolated from society. Maybe his mind never learned the constructs of human language. Maybe his mind is free to think in feelings and emotional. True thoughts without translation! Maybe society makes a mockery out of him, or perhaps society looks up to him. The origins of the box man are simple. Put a baby in a box and let him grow up. Just Kidding."]
      [:p "Well I remember this one quote about math. “No one ever learned good mathematics in a vacuum” or something like that. So what are all the things that his man never learned. He’s probably not very sociable. I still haven’t quite learned how to be a very social person. I don’t know how to use computers I don’t know how to google docs! Maybe Mr. Grunwald will look away and I can try to figure it out. But if not I dunno. I guess I can just go ahead and figure it out. There must be so many collapsing thoughts. I like thinking continuously in multiple levels. Everything is more humorous when you add another dimension. Like math. How do you get people to stop understanding. That's it! The box man is not understood, but she's trying to understand him by writing about him! Cool."]
      ]
     [:h5 "Sample from The New Yorker's John McPhee"]
     [:p
      "This sample of free writing comes from John McPhee at The New Yorker, who included a sample of his free writing in "
      (external-link "https://www.newyorker.com/magazine/2013/04/29/draft-no-4" "one of his pieces")
      ":"]
          [:blockquote
      [:p "Dear Joel: You are writing, say, about a grizzly bear. No words are forthcoming. For six, seven, ten hours no words have been forthcoming. You are blocked, frustrated, in despair. You are nowhere, and that’s where you’ve been getting. What do you do? You write, ‘Dear Mother.’ And then you tell your mother about the block, the frustration, the ineptitude, the despair. You insist that you are not cut out to do this kind of work. You whine. You whimper. You outline your problem, and you mention that the bear has a fifty-five-inch waist and a neck more than thirty inches around but could run nose-to-nose with Secretariat. You say the bear prefers to lie down and rest. The bear rests fourteen hours a day. And you go on like that as long as you can. And then you go back and delete the ‘Dear Mother’ and all the whimpering and whining, and just keep the bear."]]
     [:h5 "Sample from My Write Honey Account"]
     [:p "This is an excerpt I found after digging through my posts from a few years ago. There's nothing noteworthy about it per se, other than showing how I use this product sometimes:"
      ]
     [:blockquote
      [:p "Alright, I think today will be a good day to start getting my stuff in order and being on top of everything again. The question is, how do I start to do that?"]
      [:p "Maybe I can start by feeling inspired. Watching some videos of GTD ought to be nice."]
      [:p "Then I can get my GTD list organized. I will have to live with the uncertainty that comes with understanding the restructuring process as it's going. Of course I can take breaks to go eat something."]
      [:p "Okay, so it's becoming a bit more clear. I'll watch some videos on GTD and after a few of those, I'll make some breakfast. Potatoes and schnitzel. Ribs later."]
      [:p "So, I think I've finished with GTD reorganizing. I also realize that I'm pretty lonely. I probably should have realized this like 6 months ago, but anyways..."]
      ]
     [:p "As stated before: your results may differ. And that's completely fine."]
     ]]
   (section-header
    {:text "Free Writing Exercises &amp; Prompts"
     :icon "/images/pencil.svg"
     :alt "Icon of a pencil"})
   [:section.section
    [:div.container.content.Guide
     [:h3 "Free Writing Exercises"]
     [:p "While the purpose of free writing is to help with ideation, having an aim as a guideline and not a strict objective can help with getting the creative juices flowing."]
     [:p "Here are some ideas that you can use as an aim, most of which I also tend to use myself regularly."]
     [:h5 "Take a Brain Dump"]
     [:p "Do you have a lot of thoughts swirling around the old noodle? If so, this method is for you."]
     [:p "I've used this one to great effect when trying to figure out why I might feel disoriented or that I'm not seeing the whole picture of the details of my life."]
     [:p "My favorite approach is to just write down all thoughts and concerns, then start lumping related pieces together. This is obviously much easier to do when using a digital tool than in ink, but if deciding to choose the latter medium, sticky notes are your friend."]
     [:h5 "Reflect on Today or Yesterday"]
     [:p "The more stuff you take on, the less time you have to digest and consider how its effects will ripple or have already rippled into other parts of your life."]
     [:p "Therefore, I find it particularly useful to occasionally treat the free writing process as a journal. In particular, I attempt to check in with my own feelings, thoughts and emotions on how things are going because I personally struggle at doing so without conscious effort."]
     [:h5 "Talk or Write to Someone"]
     [:p "It sometimes helps to have some idea of who you're speaking to in order to craft up the right words."]
     [:p "Whether it's a parent, your childhood best friend, your niece, a neighbor, a resident of a country far away or anyone else that may serve as inspiration, by being deliberate about who the recipient of your writing is, the manner in which you'd speak and the amount of self-censoring you'd be doing can vary tremendously. Whose eyes and judgment will be the most beneficial when attempting to craft your desired output?"]
     [:h5 "Use a Prompt"]
     [:p "On the days when you want to get going and none of the above tips help you get started, having a more deliberate aim in the form of a prompt can help you get going."]
     [:p "The following is a big list of prompts to help you for those days. Don't be shy about using them."]
     [:h3 "Free Writing Prompts"]
     [:p "Carlton Clark, a content creator at the company Freewrite, has graciously shared sixty prompts with the world, and I'll share some of my favorites on here along with some small nudges that might help you get started on each of them."]
     [:h5 "10. Write about a time you were lost."]
     [:p "This one is wonderful because lost can mean many things. It might mean you were out in the forest without a map and had to rely on the direction the sun was setting in, or it might refer to a time where it felt like you weren't progressing in any certain direction with your life, and maybe you were perfectly okay with that."]
     [:h5 "13. Write about why you write."]
     [:p "You can begin answering this one based on your motivation to write, but it's also a great opportunity to reflect on the sequences of steps that led to the development of your writing ability: being born in this century as a human being, having access to an education that others weren't granted, being cast with expectations that you should be able to write, and so on."]
     [:h5 "22. Turn the last song you listened to into a story."]
     [:p
      "Music is wonderful because, among many other reasons, they tend to form a natural bond to key parts of our lives. Was your first slow dance to K-Ci &amp; Jojo's "
      [:i "All My Life"]
      "? Was Katy Perry's "
      [:i "Firework"]
      " playing in the car when your parents took you to the county fair for the first time? Or maybe Bach's "
      [:i "Brandenburg Concertos"]
      " remind you of the trip you and your husband took to West Berlin before the wall fell? However you relate to music, this one is ripe with possibilities."]
     [:h5 "30. You live on an abandoned island, describe your morning routine."]
     [:p "Can you even describe your current morning routine, or do you wake up responding to problems as they arise like me? An abandoned island might represent isolation to some, but to others it can represent sudden liberation from existing responsibilities. Is that necessarily a good thing?"]
     [:h5 "31. You’re in a foreign country and don’t speak the native language."]
     [:p "This is wonderfully nuanced. Some countries teach English so early on that members of the population might call out native English speakers on incorrect grammar. Others find English a byproduct of evil imperialist forces and see anyone who speaks it as a peddler of capitalism. Choose your country carefully."]
     [:h5 "38. Write about a time you were uncomfortable."]
     [:p "Something as mundane as discomfort has prompted a lot of terrible behavior through the ages. When confronted with something you've done incorrectly, are you likely to respond with the silent treatment? Do you try to tackle sources of discomfort head on, accepting negative outcomes as part of the learning process?"]
     [:h5 "46. You knock louder and louder on the door, but nobody answers."]
     [:p "Are you in your ancestral homeland meeting your grandparents for the first time? Are you trying to make amends with your testy teenager after a small dust-up during dinner, and they're ignoring you? Or worse: did they climb out of their window to escape your clutches and seek refuge elsewhere? As a movable barrier between humans that refuses to budge, a metaphorical or literal unanswered door can be the start of an eerie situation or a tearful reunion."]
     [:h5 "55. You have 10 days to live."]
     [:p "This is probably the best one to really make you reflect on your priorities. Who and what are worthy of concern and care in your life, especially with your remaining 240 hours? And who and what are just sucking your time and energy away from you?"]
     [:h5 "But wait, there's more!"]
     [:p "If none of these prompts strike your fancy, you can view all of Carlton's prompts on "
      (external-link
       "https://getfreewrite.com/blogs/writing-success/writing-prompts-60-ideas-you-can-use-today"
       "his blog post at Freewrite")
      "."]]]
   (section-header
    {:text "Free Writing Software and Websites"
     :icon "/images/monitor-book.svg"
     :alt "Icon of a book displayed on a computer screen"})
   [:section.section
    [:div.content.container.Guide
     [:h3 "Purpose-Built Free Writing Software and Websites"]
     [:h5 "Paid Desktop Solution: Scrivener"]
     [:ul
      [:li "Website: " (external-link "https://www.literatureandlatte.com/scrivener/overview" "https://www.literatureandlatte.com/scrivener/overview")]
      [:li "Platform: Desktop (Windows, macOS)" ]
      [:li "Price: $49"]]
     [:p "Initially released in January 2007 and still maintained with love today, Scrivener is a widely beloved general-purpose writing program built for the desktop.  With the ability to, as they put it, \"see the forest or the trees,\" it is engineered to help you both manage and create writing content."]
     [:p "It happens to have a wonderful feature for free writing, called composition mode, which hides all interfaces to show you just your document. This is a killer feature for free writers, as ideation can sometimes be interrupted by even the most well-meaning of distractions."]
     [:div.ActionScreenshot__Wrapper
       [:p.has-text-centered.is-size-6.is-italic "Scrivener in composition mode."]
       [:div.ActionScreenshot
        [:img {:src "/images/scrivener-composition-mode.png"
               :alt "Scrivener screenshot"}]]]
     [:p ""]
     [:h5 "Paid Web Solution: 750 Words"]
     [:ul
      [:li "Website: " (external-link "https://750words.com/" "https://750words.com/")]
      [:li "Platform: Browser (desktop)" ]
      [:li "Price: 30 day trial then $5/month"]]
     [:p "Founded in December 2009, the wonderful team behind 750 Words is credited (and rightfully so) with sparking new interest in the practice of free writing."]
     [:p "With a strong focus on building streaks and rewarding writers with badges for consistent writing, it also boasts a very active community with whom you can share your pieces if you so choose."]
     [:div.ActionScreenshot__Wrapper
      [:p.has-text-centered.is-size-6.is-italic
       "A user's screenshot of 750 Words. Credit: "
       (external-link "https://profhacker.com/" "ProfHacker")]
       [:div.ActionScreenshot
        [:img {:src "/images/750-words-ss.png"
               :alt "750 Words screenshot"}]]]
     [:p ""]
     [:h5 "Free Web & Mobile Solution: Write Honey"]
     [:ul
      [:li "Website: "
       [:a {:href "/"
            :target "_blank"} "https://writehoney.com/"]]
      [:li "Platform: Browser (desktop, mobile)" ]
      [:li "Price: Free"]]
     [:p "Published in November 2016 and improved with iterations over time, thousands of users love using Write Honey, our free offering of a purpose-built software solution for free writing."]
     [:p "With a focus on distraction-free writing as well as making it easy to recall past pieces, we back up our belief in the power of free writing by delivering what we believe is a solution tailor-made for free writing."]
     [:p "And yes: there's a dark mode, too."]
     [:div.ActionScreenshot__Wrapper
       [:p.has-text-centered.is-size-6.is-italic "Write Honey in action."]
       [:div.ActionScreenshot
        [:img {:src "/images/writehoney-screenshot-2.png"
               :alt "A screenshot of Write Honey being used"}]]]
     [:p ""]
     [:h3 "Using a Word Processor For Free Writing"]
     [:p "There's a decent chance you have a word processor on your computer as is, so why not put it to use as a tool for free writing?"]
     [:p "These pieces of software might not be tailor-made for free writing, but counting words is one keystroke away, and you can also use Google as a timer by simply using \"timer\" as a query."]
     [:p "A challenge of using a word processor, however, is figuring out how to save files and not lose track of your progress. My recommendation? Save filenames prefixed with the date. For example, if I were to write something today, the filename would be something like: 2022-08-18__Paul-Writing.docx."]
     [:h5 "The People's Champion: LibreOffice Writer"]
     [:ul
      [:li "Platform: macOS, Windows, Linux, BSD" ]
      [:li "Price: Free"]]
     [:p "Free as in both freedom and beer, LibreOffice Writer is a beloved open source word processor to the many who work in open source environments like Linux and BSD, as well as those who just want a free word processor."]
     [:h5 "The One That's Probably On Your Computer: Microsoft Word"]
     [:ul
      [:li "Platform: macOS, Windows" ]
      [:li "Price: $160"]]
     [:p "This software needs no introduction. There is a decent chance you have this on your computer already, which would make the hefty price tag listed above effectively zero."]]]
   (section-header
    {:text "Getting Started With Free Writing"
     :icon "/images/laptop-coffee.svg"
     :alt "Icon of a cup of coffee next to an open laptop computer"})
   [:section.section
    [:div.content.container.Guide
     [:p "If you've made it this far, congratulations: you've certainly done your homework on free writing! Now for the big question: where are you going from here?"]
     [:p "If you're serious about giving free writing a try, here are some tips to make your efforts as successful as possible."]
     [:h3 "Three Tips for Successful Free Writing Habit Formation"]
     [:h5 "Tip 1: Discover Your Reason To Free Write"]
     [:p "Here's the thing: if you're serious about making free writing a real habit (as in you don't want to give up after a few days of starting), it helps tremendously to have a solid reason as to why you want to adopt free writing as a habit."]
     [:p "The hard part is actually coming up with your reason. Some example reasons why one may start free writing include:"]
     [:ul
      [:li "I'm free writing because "
       [:u "I want the therapeutic benefits"]]
      [:li "I'm free writing because "
       [:u "I want to finish my novel"]]
      [:li "I'm free writing because "
       [:u "I want to explore what's in my mind"]]]
     [:p "Once you've discovered your reason, put it down in writing in the form of \"I'm free writing because _________\" and place it somewhere where you'll remember to look. Perhaps as a post-it note on the fridge, or as a pinned toot on your Mastodon account."]
     [:h5 "Tip 2: Start Small, Then Gradually Increase"]
     [:p "If the default daily expectation of 750 words, or about three pages, is too daunting, what about 250 words, or even 100 words?"]
     [:p "Similarly, if you decide to use time as a constraint, you don't need to start with an hour or thirty minutes. Ten or even five minutes is completely fine to get some momentum going."]
     [:p "Just like you wouldn't start lifting weights by doing a 300-pound bench press, you shouldn't expect too much out of yourself when attempting to start building a free writing habit. Habit formation is a journey, not a destination."]
     [:h5 "Tip 3: Forgive Yourself For Slip Ups"]
     [:p "Building up a streak is nice because a strong streak can really help create a sense of reward for the consistency you've put into and the momentum you've gained towards your free writing habit."]
     [:p "A broken streak, however, can be catastrophic to your motivation levels, but only if you let it. If you've slipped up on a streak, just remember: that streak is there to serve you, not the other way around."]
     [:p "Things happen in life: you might have to respond to a family or work emergency, or you might just be exhausted after a long day and may not have the energy to tend to your free writing habit. Does that mean you've failed - or worse - that you're a failure? Absolutely not. Hop back in the next day, fresh and still driven by your reason to free write, which itself hasn't changed."]
     [:h3 "That's All, Folks!"]
     [:p "Thank you for taking the time to read this free writing guide. I hope you've had as much fun reading it as I did creating it."]
     [:p "If you have any question, comments or concerns about this guide to free writing, please let me know."]
     [:p "Otherwise, goodbye, and happy writing!"]]]
   [:section.section
    [:div.hero-body
     (let [{:keys [name avatar mini-bio]} (read-string (slurp "resources/paul-data.edn"))]
       [:div.container.content.has-text-centered.is-italic
        [:p "Written by " name]
        [:figure.image.is-128x128
         {:style "display: inline-block;"}
         [:img.is-rounded
          {:src avatar
           :alt (str "Photo of " name)
           :title name}]]
        [:p mini-bio]])]]
   (component/cta)])

(def free-writing-page
  {:power-title "Free Writing: The Complete Guide"
   :description "Understand what free writing is, view examples and understand how and where you can maximize the benefits of this immensely useful activity."
   :path "/free-writing/"
   :priority 1
   :content free-writing-page-content
   :og-image "/images/free-writing-banner.jpg"})
