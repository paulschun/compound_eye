(ns compound-eye.view.blog
  (:require
   [markdown.core :as markdown]
   [compound-eye.blog :as blog]
   [compound-eye.component :as component]
   [compound-eye.time :as time]
   ))

(def ^:private blog-title
  "Write Free: The Write Honey Blog")

;; Blog Post

(defn- header [{:keys [title posted_at location main_image]
                {:keys [name]} :author
                :as blog-post}]
  [:div
   [:section.hero.is-medium.BlogPost__Wallpaper
    {:style (str "background-image: url('" main_image "');")}
    [:div.hero-body
     [:div.container]]]
   [:section.hero.is-black
    [:div.hero-body
     [:div.container.content.has-text-centered
      [:h1.title title]
      [:p (time/zdt->readable-string-date posted_at)]
      [:p location]]]]
   ])

(defn- body [{:keys [body] :as blog-post}]
  [:section.section
   [:div.container.content.BlogPost__Content
    (markdown/md-to-html-string body)]])

(defn- author-section [{{:keys [name avatar mini-bio]} :author :as blog-post}]
  [:section.hero
   [:div.hero-body
    [:div.container.content.has-text-centered.is-italic
     [:p "Written by " name]
     [:figure.image.is-128x128
      {:style "display: inline-block;"}
      [:img.is-rounded
       {:src avatar
        :alt (str "Photo of " name)
        :title name}]]
     [:p mini-bio]]]])

(defn- blog-post-box [{:keys [title posted_at location main_image]
                           {:keys [name]} :author
                           :as blog-post}]
  [:div.has-text-centered
   [:figure.image
    [:img {:src main_image :title title :alt (str "wallpaper for " title)}]]
   [:p.subtitle title]
   [:p (time/zdt->readable-string-date posted_at)]])

(defn- blog-post-link [label {:keys [path] :as blog-post}]
  (if blog-post
    [:a {:href (:path blog-post)}
     [:div.box
      label
      (blog-post-box blog-post)]]
    [:div.box
     label
     [:div
      [:i "(None)"]]]))

(defn- pagination [blog-post]
  [:div.container
   (let [next-post (blog/next-blog-post blog-post)
         prev-post (blog/prev-blog-post blog-post)]
     [:div.columns.is-vcentered
      [:div.column (blog-post-link "&larr; Previous Post" prev-post)]
      [:div.column (blog-post-link "Next Post &rarr;" next-post)]])])

(defn- blog-post-content [blog-post]
  [:div
   (for [section [header body author-section pagination]]
     (section blog-post))
   (component/cta)])

(defn blog-post
  [{:keys [title description] {:keys [name]} :author :as blog-post}]
  {:power-title (str title " &mdash; " blog-title)
   :description description
   :content (blog-post-content blog-post)
   :og-type "article"})

;; Blog Root

(defn blog-post-preview [{:keys [title path posted_at main_image]
                          {:keys [name]} :author
                          :as blog-post}]
  [:div
   [:a {:href path}
    [:h3 title]
    [:p name]
    [:p (time/zdt->readable-string-date posted_at)]
    [:img {:src main_image
           :alt (str "Headline picture for post with title: " title)
           :title title}]]])

(defn blog-root []
  {:power-title blog-title
   :description "Write Free is Write Honey's blog."
   :path "/blog/"
   :content
   [:div
    [:section.hero.is-success
    [:div.hero-body
     [:div.container.has-text-centered
      [:h1.title "Write Free"]
      [:h2.subtitle "The Write Honey Blog"]]]]
    [:section.section
     [:div.container
      (for [blog-post (blog/all-blog-posts)]
        (blog-post-link "" blog-post))]]]})
