(ns compound-eye.component)

(defn navbar []
  [:nav.navbar.Navbar
   [:div.navbar-brand
    [:a.navbar-item {:href "/"}
     [:img
      {:src "/images/wh-logo.svg"
       :title "Write Honey"
       :alt "Write Honey Logo"}]]
    [:a.navbar-burger {:role "button"
                       :aria-label "menu"
                       :aria-expanded "false"}
     (->> (repeat [:span {:aria-hidden "true"}])
          (take 3))]]
   [:div.navbar-menu
    [:div.navbar-end
     [:a.navbar-item.Navbar__Item
      {:href "/product/"}
      "Product Overview"]
     [:a.navbar-item.Navbar__Item.Navbar__Item--BlueHover
      {:href "/free-writing/"}
      "Free Writing"]
     [:a.navbar-item.Navbar__Item.Navbar__Item--GreenHover
      {:href "/blog/"}
      "Blog"]
     [:a.navbar-item.Navbar__Item.Navbar__Item--RedHover
      {:href "https://app.writehoney.com/register"
       :target "_blank"}
      "Create Account"]
     [:div.navbar-item
      [:a.button.is-black.is-rounded
       {:href "https://app.writehoney.com/login"
        :rel "nofollow"
        :target "_blank"}
        "Sign In"]]]]])

(defn footer []
  [:div
   [:footer.footer
    [:div.container
     [:div.columns
      [:div.column.is-two-fifths
       [:a {:href "/"}
        [:img.Footer__Logo
         {:src "/images/wh-logo.svg"
          :title "Write Honey"
          :alt "Write Honey Logo"}]]]
      [:div.column
       [:ul.Footer__Links
        [:li [:a {:href "/"} "Home"]]
        [:li [:a {:href "/product/"} "Product Overview"]]
        [:li [:a {:href "/free-writing/"} "Free Writing"]]
        [:li [:a {:href "/750-words-free-alternative/"} "750 Words Free Alternative?"]]]]
      [:div.column
       [:ul.Footer__Links
        [:li [:a {:href "/blog/"} "Blog"]]]]
      [:div.column
       [:ul.Footer__Links
        [:li [:a
              {:href "https://app.writehoney.com/register"
               :rel "nofollow"
               :target "_blank"}
              "Register"]]
        [:li [:a
              {:href "https://app.writehoney.com/login"
               :rel "nofollow"
               :target "_blank"}
              "Sign In"]]]]]]]
   [:div.DeeperFooter__Wrapper
    [:div.DeeperFooter.is-size-7
     [:div.container
      [:p.has-text-centered
       "Copyright &copy; 2022-2024 Paul S. Chun. All rights reserved."]]]]])

(defn cta []
  [:section.hero.is-medium.is-primary
   [:div.hero-body.has-text-centered
    [:div
     [:p.subtitle
      "Use the free writing app that's free for everyone, forever, and get in on all the benefits of free writing."]
     [:a.button.is-black.is-rounded.is-medium
      {:href "https://app.writehoney.com/register"
       :target "_blank"}
      "Create Your Free Account"]]]])
