(ns compound-eye.sitemap
  (:require
   [clojure.data.xml :as xml]
   [clojure.string :as str]))

(def ^:private url-base
  "https://writehoney.com")

(defn page-record->sitemap-url [{:keys [path priority]}]
  [:url
   [:loc (str url-base path)]
   [:priority priority]])

(defn- text-file? [{:keys [path] :as page-record}]
  (str/ends-with? path ".txt"))

(defn generate-sitemap [page-record-list]
  (as-> page-record-list $
    (remove text-file? $)
    (map page-record->sitemap-url $)
    (sort-by :priority $)
    (reverse $)
    (xml/sexp-as-element [:urlset {:xmlns "http://www.sitemaps.org/schemas/sitemap/0.9"} $])
    (xml/emit-str $)))

(defn wrap-sitemap [page-record-list]
  (conj page-record-list {:path "/sitemap.xml"
                          :priority 0.1
                          :output (generate-sitemap page-record-list)}))
